<?php

class Hello extends CI_Controller{
  public function index(){

    $this->load->model('Hello_model');
    $model = $this->Hello_model;
    $s = $model->str;

    $data['teks'] = $s;

    $this->load->view('helloview', $data);
  }
}
