<?php
class Lingkaran extends CI_Controller{
  public function index(){
    if(isset($_POST['btnSubmit'])){
      $radius = $_POST['jari2'];

      include(APPPATH . "models\\Lingkaran_model.php");

      $model = new Lingkaran_model($radius);

      $this->load->view('lingkaranview', array('model' => $model));
    } else{
      $this->load->view('lingkaran_form_view');
    }
  }
}
